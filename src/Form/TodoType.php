<?php

namespace App\Form;


use App\Entity\Todo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TodoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('value', TextType::class,[
                'label' => 'A faire ',
            ])
            ->add('Date',DateType::class,[
                'years' => range((int) date('Y'), (int) date('Y')+1),
                'label' => 'Le ',
            ])
            ->add('Hour',TimeType::class,[
                'label'=> 'Heure',
            ])
            ->add('color',ChoiceType::class,[
                'choices'=>[
                    'Normal' => 'Normal',
                    'Important' => 'Important',
                    'Urgent' => 'Urgent'
                ],
                'label' => 'Importance '
            ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
            'data_class' => Todo::class
            ]);
    }
}
