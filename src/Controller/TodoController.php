<?php

namespace App\Controller;

use App\Entity\Todo;
use App\Form\TodoType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\Response;

class TodoController extends AbstractController
{
    /**
     * @Route("", name="todo")
     */
    public function index(Request $request)
    {
        if ($this->getUser()) {
            $todo = new Todo();
            $todo->setUser($this->getUser());
            $todo->setDate(new \DateTime());
            $todo->setHour(new \DateTime());
            $form = $this->createForm(TodoType::class, $todo);
            $form->handleRequest($request);
            $data = $this->getDoctrine()
                ->getRepository(Todo::class)
                ->findBy(array('user' => $this->getUser()), array('Date' => 'ASC'));
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($todo);
                $entityManager->flush();
                return $this->redirectToRoute('todo');
            }
//            var_dump($this->getUser());
            return $this->render('todo/index.html.twig', [
                'test' => $form->createView(),
                'data' => $data,
                'date' => new \DateTime(),
                'user' => $this->getUser()
            ]);
        } else {
            return $this->redirectToRoute('app_login');
        }
    }

    /**
     * @Route("/delete/{id}",name="delete")
     */
    public function delete(int $id)
    {
        if ($this->getUser()) {
            $data = $this->getDoctrine()
                ->getRepository(Todo::class)
                ->findBy(array('user' => $this->getUser(), 'id' => $id));
            if (count($data) !== 0) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($data[0]);
                $entityManager->flush();
            }
            return $this->redirectToRoute("todo");
        } else {
            return $this->redirectToRoute('app_login');
        }
    }

    /**
     * @Route("/modify/{id}",name="modify")
     */
    public function modify(int $id, Request $request)
    {
        if ($this->getUser()) {
            $data = $this->getDoctrine()
                ->getRepository(Todo::class)
                ->findBy(array('user' => $this->getUser(), 'id' => $id));
            if (count($data) !== 0) {
                $form = $this->createForm(TodoType::class, $data[0]);
                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->flush();
                    return $this->redirectToRoute('todo');
                }
                return $this->render('todo/index.html.twig',
                    ['test' => $form->createView(),
                        'data' => $data,
                        'date' => new \DateTime(),
                        'user' => $this->getUser()
                    ]);
            }
            return $this->redirectToRoute('todo');
        } else {
            return $this->redirectToRoute('app_login');
        }
    }
}
